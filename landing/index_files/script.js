$(document).ready(function() {
	"use strict";
// Adjust Same Height of Left Block and Right Block
     var leftHeight = $('#left-block').height();
     var rightHeight = $('#right-block').height();

     var width = $(window).width();
      if (width > 767) {
         if (leftHeight > rightHeight) {
            rightHeight = leftHeight;
         } else {
            leftHeight = rightHeight;
         }

         $('#left-block').css('height', rightHeight);
         $('#right-block').css('height', rightHeight);
      }

	$("#subscribe").click(function() {

		if($('#email').val()==''){
            new PNotify({
                title: 'Error',
                text: "El correo no puede ser vacío",
                type: 'error',
                styling: 'bootstrap3'
            });
			return;
		}
		// Expresion regular para validar el correo
		var regex = /[\w-\.]{2,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/;

		// Se utiliza la funcion test() nativa de JavaScript
		if (regex.test($('#email').val().trim())) {
			agregarCorreo();
		} else {
            new PNotify({
                title: 'Error',
                text: "El correo no es valido",
                type: 'error',
                styling: 'bootstrap3'
            });
			return;

		}






	 });

	var agregarCorreo = function () {
		var parametros = {
			'correo': $('#email').val()
		};
		$.ajax({
				type: "POST",
				url: "../admin/index.php?service=cliente&metodo=RegistrarCorreo",
				data: JSON.stringify(parametros),
				contentType: "application/json",
				success: function (respuesta) {
					var ajaxResponse = $.parseJSON(respuesta);
					var mensaje = ajaxResponse.Mensaje;
					if (ajaxResponse != null) {
						if (ajaxResponse.success == 1) {
							//$('.newsletter-success').html(mensaje).fadeIn().delay(3000).fadeOut();
                            new PNotify({
                                title: 'Listo',
                                text: mensaje,
                                type: 'success',
                                styling: 'bootstrap3'
                            });
						} else {
                            new PNotify({
                                title: 'Error',
                                text: mensaje,
                                type: 'error',
                                styling: 'bootstrap3'
                            });

							//$('.newsletter-error').html(mensaje).fadeIn().delay(3000).fadeOut();
						}
					} else {

                        new PNotify({
                            title: 'Regular Success',
                            text: mensaje,
                            type: 'success',
                            styling: 'bootstrap3'
                        });
						//$('.newsletter-error').html(mensaje).fadeIn().delay(3000).fadeOut();
					}
				}, beforeSend: function () {
					//Bloquear();

				},
				complete: function () {
					//desBloquear();
				}
			}
		)

	};

	
 });