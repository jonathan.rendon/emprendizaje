<?php
/**
 * Created by PhpStorm.
 * User: Jonathan Rendón
 * Date: 20/12/18
 * Time: 09:42 PM
 */

/** Obtener Instagram**/
$ch = curl_init();
$url = 'https://api.instagram.com/v1/users/self/media/recent/?&count=12&access_token=5687572090.a22e6cf.c7cf663aee0544a3a6a47cfeaa512861';
curl_setopt($ch, CURLOPT_URL,$url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
$resultData = curl_exec($ch);
$fotos = json_decode($resultData, true);
?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" xmlns="http://www.w3.org/1999/html"> <!--<![endif]-->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="description" content="Emprendizaje hispano">
    <meta name="keywords" content="Cursos ,Taxes, Emprendimiento, Hispanos en Usa, QuickBooks, Talleres">
    <meta name="author" content="Jonathan Rendón">
    <title>Emprendizaje Hispano</title>

    <!-- Mobile Specific Meta
    ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="favicon-16x16.png">
    <link rel="manifest" href="site.webmanifest">
    <link rel="mask-icon" href="safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#2d89ef">
    <meta name="theme-color" content="#ffffff">


    <!-- CSS
    ================================================== -->
    <!-- Fontawesome Icon font -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- bootstrap.min css -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Animate.css -->
    <link rel="stylesheet" href="css/animate.css">
    <!-- Owl Carousel -->
    <link rel="stylesheet" href="css/owl.carousel.css">
    <!-- Grid Component css -->
    <link rel="stylesheet" href="css/component.css">
    <!-- Slit Slider css -->
    <link rel="stylesheet" href="css/slit-slider.css">
    <!-- Main Stylesheet -->
    <link rel="stylesheet" href="css/main.css">
    <!-- Media Queries -->
    <link rel="stylesheet" href="css/media-queries.css">

    <!--
    Google Font
    =========================== -->

    <!-- Oswald / Title Font -->
    <link href='http://fonts.googleapis.com/css?family=Oswald:400,300' rel='stylesheet' type='text/css'>
    <!-- Ubuntu / Body Font -->
    <link href='http://fonts.googleapis.com/css?family=Ubuntu:400,300' rel='stylesheet' type='text/css'>

    <!-- Modernizer Script for old Browsers -->
    <script src="js/modernizr-2.6.2.min.js"></script>
    <!--Start of Tawk.to Script-->
    <script type="text/javascript">

    </script>
    <!--End of Tawk.to Script-->

</head>

<body id="body">
<!--
Start Preloader
==================================== -->
<div id="loading-mask">
    <div class="loading-img">
        <img  src="img/logom.png" class="hidden-lg"/>
        <img  src="img/logo.png" class="hidden-xs hidden-md hidden-sm"/>
    </div>
</div>
<!--
End Preloader
==================================== -->

<!--
Fixed Navigation
==================================== -->
<header id="navigation" class="navbar navbar-inverse">
    <div class="container">
        <div class="navbar-header">
            <!-- responsive nav button -->
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <!-- /responsive nav button -->

            <!-- logo -->
            <a class="navbar-brand" href="#body">
                <h1 id="logo">
                    <img src="img/logom.png" alt="Emprendizaje hispano"/>
                </h1>
            </a>
            <!-- /logo -->
        </div>

        <!-- main nav -->
        <nav class="collapse navbar-collapse navbar-right" role="Navigation">
            <ul id="nav" class="nav navbar-nav">
                <li class="current"><a href="#body">Inicio</a></li>
                <li><a href="#services">¿Qué hacemos?</a></li>
                <li><a href="#showcase">Galería</a></li>
                <li><a href="#our-team">¿Quiénes somos?</a></li>
                <li><a href="#contact-us">Contacto</a></li>
            </ul>
        </nav>
        <!-- /main nav -->

    </div>
</header>
<!--
End Fixed Navigation
==================================== -->
<!--
Welcome Slider
==================================== -->
<section id="home">
    <!-- <div align="center" class="embed-responsive embed-responsive-16by9">
         <video autoplay loop muted class="embed-responsive-item">
             <source src="img/video1.mp4" type="video/mp4">
         </video>
     </div>-->
    <div id="slitSlider" class="sl-slider-wrapper">
        <div class="sl-slider">

            <!-- single slide item -->
            <div class="sl-slide" data-orientation="horizontal" data-slice1-rotation="-25" data-slice2-rotation="-25" data-slice1-scale="2" data-slice2-scale="2">
                <div class="sl-slide-inner">
                    <div class="bg-img bg-img-1"></div>
                    <div class="carousel-caption">
                        <div>
                            <h2 data-wow-duration="500ms"  data-wow-delay="500ms" class="heading animated fadeInRight">Bienvenidos</h2>
                            <h3 class="animated fadeInUp">conoce nuestra academia</h3>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /single slide item -->

            <!-- single slide item -->
            <div class="sl-slide" data-orientation="vertical" data-slice1-rotation="10" data-slice2-rotation="-15" data-slice1-scale="1.5" data-slice2-scale="1.5">
                <div class="sl-slide-inner">
                    <div class="bg-img bg-img-2"></div>
                    <div class="carousel-caption">
                        <div>
                            <h2 class="heading animated fadeInDown">Aprende en tu idioma</h2>
                            <h3 class="animated fadeInUp">con destacados profesionales</h3>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /single slide item -->

            <!-- single slide item -->
            <div class="sl-slide" data-orientation="horizontal" data-slice1-rotation="3" data-slice2-rotation="3" data-slice1-scale="2" data-slice2-scale="1">
                <div class="sl-slide-inner">
                    <div class="bg-img bg-img-3"></div>
                    <div class="carousel-caption">
                        <div>
                            <h2 class="heading animated fadeInRight">Varios niveles de conocimiento</h2>
                            <h3 class="animated fadeInLeft">aprendizaje efectivo</h3>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /single slide item -->

        </div><!-- /sl-slider -->

        <nav id="nav-arrows" class="nav-arrows">
            <span class="nav-arrow-prev">Previous</span>
            <span class="nav-arrow-next">Next</span>
        </nav>

        <nav id="nav-dots" class="nav-dots">
            <span class="nav-dot-current"></span>
            <span></span>
            <span></span>
        </nav>

    </div><!-- /slider-wrapper -->
</section>
<!--/#home section-->


<!-- Start Services Section
==================================== -->

<section id="services" class="bg-one">

    <div class="container">
        <div class="row wow fadeInDown" data-wow-duration="500ms">
            <div class="col-lg-12">

                <!-- section title -->
                <div class="title text-center">
                    <h2> <span class="color"> Cursos</span></h2>
                    <div class="border"></div>
                </div>
                <!-- /section title -->
                <!-- portfolio item filtering -->
                <div class="portfolio-filter clearfix">
                    <ul class="text-center">
                        <li><a href="javascript:void(0)" class="filter" data-filter="all"><i class="fa fa-folder fa-1x"></i> Todos</a></li>
                        <li><a href="javascript:void(0)" class="filter" data-filter=".presenciales"><i class="fa fa-building fa-1x"></i> Presenciales</a></li>
                        <li><a href="javascript:void(0)" class="filter" data-filter=".online"><i class="fa fa-desktop fa-1x"></i> Online</a></li>
                        <li><a href="javascript:void(0)" class="filter" data-filter=".finanzas"><i class="fa fa-money fa-1x"></i> Finanzas</a></li>
                        <li><a href="javascript:void(0)" class="filter" data-filter=".comunicacion"><i class="fa fa-bullhorn fa-1x"></i> Comunicación</a></li>
                        <li><a href="javascript:void(0)" class="filter" data-filter=".oficios"><i class="fa fa-users fa-1x"></i> Oficios</a></li>
                    </ul>
                </div>
                <!-- /portfolio item filtering -->

            </div> <!-- /end col-lg-12 -->
        </div> <!-- end row -->
    </div>	<!-- end container -->

    <!-- portfolio items -->
    <div class="portfolio-item-wrapper wow fadeInUp" data-wow-duration="500ms">
        <ul id="og-grid" class="og-grid">

            <!-- single portfolio item -->
            <li class="mix comunicacion presenciales">
                <a data-largesrc="img/portfolio/redes.jpg" data-title="Redes sociales para emprendedores"
                   data-description='
							<p style="text-align: justify">	Contenido :</p>

							<p style="text-align: justify">
								<i class="fa fa-info-circle fa-xs"></i>
							<span>Introducción.</span>
							</p>
							<p style="text-align: justify">
								<i class="fa fa-info-circle fa-xs"></i>
							<span>Redes sociales: cúales son y su importancia.</span>
							</p>
							<p style="text-align: justify">
							<i class="fa fa-info-circle fa-xs"></i>
							<span>Planificación y estrategia:<br>
							     <spam style="padding-left:20px;">-Elección de las redes a utilizar.</span><br>
							     <spam style="padding-left:20px;">-Configuración básica.</span><br>
							     <spam style="padding-left:20px;">-Planificación mensual / Manual de contenido / Grilla.</span>
							</span>
							</p>
							<p style="text-align: justify">
								<i class="fa fa-info-circle fa-xs"></i>
								<span> Facebook:<br>
							       <spam style="padding-left:20px;">-Diferencia entre fan page, perfil y grupos.</span><br>
							       <spam style="padding-left:20px;">-Crear y Administrar una fan page.</span><br>
							       <spam style="padding-left:20px;">-Nombre de usuario y verificación de la página.</span><br>
							       <spam style="padding-left:20px;">-Roles.</span><br>
							       <spam style="padding-left:20px;">-Panel de administración.</span><br>
							       <spam style="padding-left:20px;">-Personalización.</span><br>
							       <spam style="padding-left:20px;">-Publicación.</span><br>
							       <spam style="padding-left:20px;">-Mensajería.</span><br>
							       <spam style="padding-left:20px;">-Consigue tus primeros likes.</span><br>
							       <spam style="padding-left:20px;">-Estadísticas.</span><br>
							       <spam style="padding-left:20px;">-Facebook Ads.</span>
							    </span>
							</p>
							<p style="text-align: justify">
								<i class="fa fa-info-circle fa-xs"></i>
								<span> Instagram:<br>
							       <spam style="padding-left:20px;">-Ventajas del uso de Instagram.</span><br>
							       <spam style="padding-left:20px;">-Creación y administración de una cuenta para empresa.</span><br>
							       <spam style="padding-left:20px;">-Consejos y trucos.</span><br>
							       <spam style="padding-left:20px;">-Perfil, biografía.</span><br>
							       <spam style="padding-left:20px;">-Fotos / videos.</span><br>
							       <spam style="padding-left:20px;">-Hashtags.</span><br>
							       <spam style="padding-left:20px;">-Geotags.</span><br>
							       <spam style="padding-left:20px;">-Stories.</span><br>
							       <spam style="padding-left:20px;">-Instagram shopping.</span><br>
							       <spam style="padding-left:20px;">-Campañas (creación, manejo, resultados).</span>
							    </span>
							</p>'>
                    <div class="contenedor">
                        <img src="img/portfolio/redes.jpg" alt="Redes Sociales">
                        <div class="centrado"><h3 class="color_blue title-curso">REDES SOCIALES</h3></div>
                    </div>
                    <div class="hover-mask">
								<span>
									<i class="fa fa-plus fa-2x"></i>
								</span>
                    </div>
                </a>
            </li>
            <!-- single portfolio item -->
            <li class="mix finanzas presenciales">
                <a data-largesrc="img/portfolio/quickbook.jpg" data-title="Quickbooks"
                   data-description='<p style="text-align: justify">Software o sistema contable donde puedes llevar el Control de las Finanzas de tu Negocio.</p>

							<p style="text-align: justify">	Es el Software más usado por los pequeños y medianos negocios.</p>

							<p style="text-align: justify">	Contenido :</p>

							<p style="text-align: justify">
								<i class="fa fa-info-circle fa-xs"></i>
							<span>Crear su empresa.</span>
							</p>
							<p style="text-align: justify">
								<i class="fa fa-info-circle fa-xs"></i>
							<span>Transacciones diarias.</span>
							</p>
							<p style="text-align: justify">
							<i class="fa fa-info-circle fa-xs"></i>
							<span>Ventas: facturas, recibos de ventas, pagos, depósitos, notas de crédito y estados de cuenta.</span>
							</p>
							<p style="text-align: justify">
								<i class="fa fa-info-circle fa-xs"></i>
								<span>Compra: facturas, pagos de facturas, cheques, recibos de pago de facturas, facturas vs. cheques y notas de crédito.</span>
							</p>

							<p style="text-align: justify">
								<i class="fa fa-info-circle fa-xs"></i>
								<span>Inventario: órdenes de compra, recibo de artículos, ajuste de inventario.</span>
							</p>

							<p style="text-align: justify">
								<i class="fa fa-info-circle fa-xs"></i>
								<span>Impuestos sobre las ventas: configurar, ajustar, pagar y los informes de impuestos de ventas.</span>
							</p>
							<p style="text-align: justify">
								<i class="fa fa-info-circle fa-xs"></i>
								<span>Bancarios: conciliación bancaria y localizar.</span>
							</p>
							<p style="text-align: justify">
								<i class="fa fa-info-circle fa-xs"></i>
								<span>Dar y recibir reembolsos.</span>
							</p>
							<p style="text-align: justify">
								<i class="fa fa-info-circle fa-xs"></i>
								<span>Eliminar un cheque.</span>
							</p>
							<p style="text-align: justify">
								<i class="fa fa-info-circle fa-xs"></i>
								<span>Consejos y trucos.</span>
							</p>
							<p style="text-align: justify">
								<i class="fa fa-info-circle fa-xs"></i>
								<span>Limpiar listas (borrar, ocultar y fusionar).</span>
							</p>
							<p style="text-align: justify">
								<i class="fa fa-info-circle fa-xs"></i>
								<span>Ordenar y personalizar las listas.</span>
							</p>
							<p style="text-align: justify">
								<i class="fa fa-info-circle fa-xs"></i>
								<span>estados financieros "colapso".</span>
							</p>
							<p style="text-align: justify">
								<i class="fa fa-info-circle fa-xs"></i>
								<span>Mostrar P & L por mes o como un porcentaje de los ingresos.</span>
							</p>
							<p style="text-align: justify">
								<i class="fa fa-info-circle fa-xs"></i>
								<span>Comprender Contabilidad.</span>
							</p>'>
                    <div class="contenedor">
                        <img src="img/portfolio/quickbook.jpg" alt="Quickbooks">
                        <div class="centrado"><h3 class="color_blue title-curso">QUICKBOOKS</h3></div>
                    </div>
                    <div class="hover-mask">
								<span>
									<i class="fa fa-plus fa-2x"></i>
								</span>
                    </div>
                </a>
            </li>
            <!-- /single portfolio item -->
            <li class="mix finanzas presenciales">
                <a data-largesrc="img/portfolio/quickbook2.jpg" data-title="Quickbooks avanzado"
                   data-description='<p style="text-align: justify">Software o sistema contable donde puedes llevar el Control de las Finanzas de tu Negocio.</p>

							<p style="text-align: justify">	Es el Software más usado por los pequeños y medianos negocios.</p>

							<p style="text-align: justify">	Contenido :</p>

							<p style="text-align: justify">
								<i class="fa fa-info-circle fa-xs"></i>
								<span>Chequear las dudas que tengas del Quickbooks Básico.</span>
							</p>
							<p style="text-align: justify">
								<i class="fa fa-info-circle fa-xs"></i>
								<span>Agregar activos y pasivos.</span>
							</p>
							<p style="text-align: justify">
								<i class="fa fa-info-circle fa-xs"></i>
								<span>Reconciliación bancaria.</span>
							</p>
							<p style="text-align: justify">
								<i class="fa fa-info-circle fa-xs"></i>
								<span>Tomar pagos  y muchos más.</span>
							</p>'>
                    <div class="contenedor">
                        <img src="img/portfolio/quickbook2.jpg" alt="Quickbooks avanzado">
                        <div class="centrado"><h3 class="color_blue title-curso">QUICKBOOKS AVANZADO</h3></div>
                    </div>
                    <div class="hover-mask">
								<span>
									<i class="fa fa-plus fa-2x"></i>
								</span>
                    </div>
                </a>
            </li>
            <!-- /single portfolio item -->

            <!-- /single portfolio item -->
            <li class="mix finanzas presenciales">
                <a data-largesrc="img/portfolio/payroll.jpg" data-title="Payroll"
                   data-description='<p style="text-align: justify">	Contenido :</p>

							<p style="text-align: justify">
								<i class="fa fa-info-circle fa-xs"></i>
								<span>Aprender sobre la nómina.</span>
							</p>
							<p style="text-align: justify">
								<i class="fa fa-info-circle fa-xs"></i>
								<span>Las obligaciones.</span>
							</p>
							<p style="text-align: justify">
								<i class="fa fa-info-circle fa-xs"></i>
								<span>Reportes Federales.</span>
							</p>
							<p style="text-align: justify">
								<i class="fa fa-info-circle fa-xs"></i>
								<span>Cálculo de taxes.</span>
							</p>'>
                    <div class="contenedor">
                        <img src="img/portfolio/payroll.jpg" alt="Payroll">
                        <div class="centrado"><h3 class="color_blue title-curso">PAYROLL</h3></div>
                    </div>
                    <div class="hover-mask">
								<span>
									<i class="fa fa-plus fa-2x"></i>
								</span>
                    </div>
                </a>
            </li>
            <!-- /single portfolio item -->
            <!-- /single portfolio item -->
            <li class="mix finanzas presenciales">
                <a data-largesrc="img/portfolio/finanzas.jpg" data-title="Finanzas"
                   data-description='<p style="text-align: justify">	Contenido :</p>
							<p style="text-align: justify">
								<i class="fa fa-info-circle fa-xs"></i>
								<span>ABC de las finanzas.</span>
							</p>
							<p style="text-align: justify">
								<i class="fa fa-info-circle fa-xs"></i>
								<span>Transforma tus finanzas.</span>
							</p>
							<p style="text-align: justify">
								<i class="fa fa-info-circle fa-xs"></i>
								<span>Cómo salir de deudas.</span>
							</p>
							<p style="text-align: justify">
								<i class="fa fa-info-circle fa-xs"></i>
								<span>Fondo de emergencia.</span>
							</p>
							<p style="text-align: justify">
								<i class="fa fa-info-circle fa-xs"></i>
								<span>Fondo de los 6 meses de gasto.</span>
							</p>
							<p style="text-align: justify">
								<i class="fa fa-info-circle fa-xs"></i>
								<span>Inversión.</span>
							</p>
							<p style="text-align: justify">
								<i class="fa fa-info-circle fa-xs"></i>
								<span>Quién debe recibir pago de payrrol.</span>
							</p>'>
                    <div class="contenedor">
                        <img src="img/portfolio/finanzas.jpg" alt="Finanzas">
                        <div class="centrado"><h3 class="color_blue title-curso">FINANZAS</h3></div>
                    </div>
                    <div class="hover-mask">
								<span>
									<i class="fa fa-plus fa-2x"></i>
								</span>
                    </div>
                </a>
            </li>
            <!-- /single portfolio item -->
            <!-- /single portfolio item -->
            <li class="mix comunicacion presenciales">
                <a data-largesrc="img/portfolio/oratoria.jpg" data-title="Oratoria"
                   data-description='
							<p style="text-align: justify">	Contenido :</p>

							<p style="text-align: justify">
								<span>El taller incluye la teoría y la práctica con las herramientas básicas de la oratoria: respiración diafragmática, manejo de la voz, buena dicción, pronunciación, actitud al hablar. Ejercicios básicos y otra serie de contenidos y tips para mejorar tu desempeño expresivo.   </span>
							</p>'>
                    <div class="contenedor">
                        <img src="img/portfolio/oratoria.jpg" alt="Oratoria">
                        <div class="centrado"><h3 class="color_blue title-curso">ORATORIA</h3></div>
                    </div>
                    <div class="hover-mask">
                        <h3 class="color_blue">Oratoria</h3>
                        <span>
									<i class="fa fa-plus fa-2x"></i>
								</span>
                    </div>
                </a>
            </li>
            <!-- /single portfolio item -->
            <!-- /single portfolio item -->
            <li class="mix comunicacion presenciales">
                <a data-largesrc="img/portfolio/presentaciones.jpg" data-title="Presentaciones orales existosas"
                   data-description='
							<p style="text-align: justify">	Contenido :</p>

							<p style="text-align: justify">
								<span>AAcabar con el miedo escénico es uno de los objetivos de este curso que presenta técnicas y prácticas concretas para mejorar la capacidad expresiva y habilidades de presentación oral. Se centra en aspectos vitales para manejarse como un orador exitoso: presencia, actitud, credibilidad, control sobre la audiencia.</span>
							</p>'>
                    <div class="contenedor">
                        <img src="img/portfolio/presentaciones.jpg" alt="Presentaciones orales existosas">
                        <div class="centrado"><h3 class="color_blue title-curso">PRESENTACIONES</h3></div>
                    </div>
                    <div class="hover-mask">
                                <span>
									<i class="fa fa-plus fa-2x"></i>
								</span>
                    </div>
                </a>
            </li>
            <!-- /single portfolio item -->
            <!-- /single portfolio item -->
            <li class="mix comunicacion presenciales">
                <a data-largesrc="img/portfolio/atencion.jpg" data-title="Atención al cliente"
                   data-description='
							<p style="text-align: justify">	Contenido :</p>

							<p style="text-align: justify">
								<span>El cliente siempre tiene la razón y es la razón de ser de las empresas. Bajo estas premisas el curso contempla las herramientas técnicas necesarias para el manejo de clientes, reconocimiento de los tipos de cliente, manejo de emociones y atención personalizada. </span>
							</p>'>
                    <div class="contenedor">
                        <img src="img/portfolio/atencion.jpg" alt="Atención al cliente">
                        <div class="centrado"><h3 class="color_blue title-curso">ATENCIÓN AL CLIENTE</h3></div>
                    </div>

                    <div class="hover-mask">
                                <span>
									<i class="fa fa-plus fa-2x"></i>
								</span>
                    </div>
                </a>
            </li>
            <!-- /single portfolio item -->
            <!-- /single portfolio item -->
            <li class="mix comunicacion presenciales">
                <a data-largesrc="img/portfolio/produccion.jpg" data-title="Producción y dirección de comerciales para la televisión"
                   data-description='
							<p style="text-align: justify">	Contenido :</p>

							<p style="text-align: justify">
								<span>Si siempre quisiste conocer cómo se concibe, planifica y realiza un comercial para la televisión esta es una buena oportunidad. ¿Qué es un comercial para la TV? ¿Cómo se planifica? ¿Quién es quién en la producción? ¿Qué necesitamos para hacer un comercial? ¿Quién nos da la idea? Respuestas a esas preguntas y muchas más las conocerás en nuestro curso.</span>
							</p>'>
                    <div class="contenedor">
                        <img src="img/portfolio/produccion.jpg" alt="Producción y dirección de comerciales para la televisión">
                        <div class="centrado"><h3 class="color_blue title-curso">COMERCIALES TV</h3></div>
                    </div>

                    <div class="hover-mask">
                                <span>
									<i class="fa fa-plus fa-2x"></i>
								</span>
                    </div>
                </a>
            </li>
            <!-- /single portfolio item -->
            <!-- /single portfolio item -->
            <li class="mix comunicacion presenciales">
                <a data-largesrc="img/portfolio/mercadeo.jpg" data-title="Mercadeo"
                   data-description='
							<p style="text-align: justify">	Contenido :</p>

							<p style="text-align: justify">
								<span>Los cuatro mandamientos del mercadeo: El mercadeo es una herramienta muy necesaria en estos tiempos. Tiene principios y lineamientos que deben respetarse para tener éxito en los negocios. El curso se basa en estas cuatro categorías o mandamientos:</span><br>
							    <span style="padding-left:20px;">-Conoce tu empresa.</span><br>
							    <span style="padding-left:20px;">-Conoce tu cliente.</span><br>
							    <span style="padding-left:20px;">-Conoce tu producto.</span><br>
							    <span style="padding-left:20px;">-Conoce tu competencia.</span>
							</p>'>
                    <div class="contenedor">
                        <img src="img/portfolio/mercadeo.jpg" alt="Mercadeo">
                        <div class="centrado"><h3 class="color_blue title-curso">MERCADEO</h3></div>
                    </div>

                    <div class="hover-mask">
                                <span>
									<i class="fa fa-plus fa-2x"></i>
								</span>
                    </div>
                </a>
            </li>
            <!-- /single portfolio item -->
            <!-- /single portfolio item -->
            <li class="mix oficios presenciales">
                <a data-largesrc="img/portfolio/excel.jpg" data-title="Excel"
                   data-description='
							<p style="text-align: justify">	Contenido :</p>

							<p style="text-align: justify">
								<span>Excel básico, medio y avanzado:</span>
							</p>
							<p style="text-align: justify">
								<span>Dirigido a todas aquellas personas que como emprendedores necesitan conocer las herramientas para la elaboración de un plan financiero para su negocio. El curso incluye la elaboración práctica de una tabla financiera para un modelo de negocio.</span>
							</p>'>
                    <div class="contenedor">
                        <img src="img/portfolio/excel.jpg" alt="Excel">
                        <div class="centrado"><h3 class="color_blue title-curso">EXCEL</h3></div>
                    </div>

                    <div class="hover-mask">
                                <span>
									<i class="fa fa-plus fa-2x"></i>
								</span>
                    </div>
                </a>
            </li>
            <!-- /single portfolio item -->
            <!-- /single portfolio item -->
            <li class="mix comunicacion presenciales">
                <a data-largesrc="img/portfolio/guiones.jpg" data-title="Elaboración de guiones"
                   data-description='
							<p style="text-align: justify">	Contenido :</p>

							<p style="text-align: justify">
								<span>Dirigido a los interesados en conocer los principios, técnicas y la realización de guiones para programas de radio, televisión y medios afines. Es una herramienta esencial para todos aquellos motivados a la producción profesional en estos medios.</span>
							</p>'>
                    <div class="contenedor">
                        <img src="img/portfolio/guiones.jpg" alt="Elaboración de guiones">
                        <div class="centrado"><h3 class="color_blue title-curso">ELABORACIÓN DE GUIONES</h3></div>
                    </div>

                    <div class="hover-mask">
                                <span>
									<i class="fa fa-plus fa-2x"></i>
								</span>
                    </div>
                </a>
            </li>
            <!-- /single portfolio item -->
            <!-- /single portfolio item -->
            <li class="mix comunicacion presenciales">
                <a data-largesrc="img/portfolio/feliz.jpg" data-title="El arte de ser feliz"
                   data-description='
							<p style="text-align: justify">	Contenido :</p>

							<p style="text-align: justify">
								<span>En un mundo de cambios permanentes y repentinos nos vemos abrumados por la realidad y a veces no sabemos cómo enfrentarla, por lo que se hace necesario este curso.</span>
							</p>
							<p style="text-align: justify">
								<span>En él se manejan los elementos básicos para controlar las emociones y el conocimiento de la Psicología en el desarrollo de la actitud positiva que oriente a canalizar la motivación y el camino al éxito.</span>
							</p>'>
                    <div class="contenedor">
                        <img src="img/portfolio/feliz.jpg" alt="El arte de ser feliz">
                        <div class="centrado"><h3 class="color_blue title-curso">EL ARTE DE SER FELIZ</h3></div>
                    </div>

                    <div class="hover-mask">
                                <span>
									<i class="fa fa-plus fa-2x"></i>
								</span>
                    </div>
                </a>
            </li>
            <!-- /single portfolio item -->



        </ul> <!-- end og grid -->
    </div>  <!-- portfolio items wrapper -->

    <div class="container">
        <div class="row">

            <!-- section title -->
            <div class="title text-center wow fadeIn" data-wow-duration="1500ms" >
                <h2><span class="color">Asesoría</span></h2>
                <div class="border"></div>
            </div>
            <!-- /section title -->

            <!-- About item -->
            <div class="col-md-4 text-center wow fadeInUp" data-wow-duration="500ms" >
                <div class="wrap-about">
                    <div class="icon-box">
                        <i class="fa fa-file-text-o fa-4x"></i>
                    </div>
                    <!-- Express About Yourself -->
                    <div class="about-content text-center">
                        <h3 class="color_blue">Impuestos</h3>
                    </div>
                </div>
            </div>
            <!-- End About item -->

            <!-- About item -->
            <div class="col-md-4 text-center wow fadeInUp" data-wow-duration="500ms" data-wow-delay="250ms">
                <div class="wrap-about">
                    <div class="icon-box">
                        <i class="fa fa-cogs fa-4x"></i>
                    </div>
                    <!-- Express About Yourself -->
                    <div class="about-content text-center">
                        <h3 class="color_blue">Incorporar entidades</h3>
                    </div>
                </div>
            </div>
            <!-- End About item -->

            <!-- About item -->
            <div class="col-md-4 text-center wow fadeInUp" data-wow-duration="500ms" data-wow-delay="500ms">
                <div class="wrap-about kill-margin-bottom">
                    <div class="icon-box">
                        <i class="fa fa-list-alt fa-4x"></i>
                    </div>
                    <!-- Express About Yourself -->
                    <div class="about-content text-center">
                        <h3 class="color_blue">Marketing</h3>
                    </div>
                </div>
            </div>
            <!-- End About item -->

            <!-- About item -->
            <div class="col-md-4 text-center wow fadeInUp" data-wow-duration="500ms" data-wow-delay="750ms">
                <div class="wrap-about kill-margin-bottom">
                    <div class="icon-box">
                        <i class="fa fa-bar-chart-o fa-4x"></i>
                    </div>
                    <!-- Express About Yourself -->
                    <div class="about-content text-center">
                        <h3 class="color_blue">Plan de Negocios</h3>
                    </div>
                </div>
            </div>
            <!-- End About item -->
            <!-- About item -->
            <div class="col-md-4 text-center wow fadeInUp" data-wow-duration="500ms" data-wow-delay="1000ms">
                <div class="wrap-about kill-margin-bottom">
                    <div class="icon-box">
                        <i class="fa fa-laptop fa-4x"></i>
                    </div>
                    <!-- Express About Yourself -->
                    <div class="about-content text-center">
                        <h3 class="color_blue">Desarrollo Web</h3>
                    </div>
                </div>
            </div>
            <!-- End About item -->

        </div>

    </div>   	<!-- End container -->
</section>   <!-- End section -->


<!-- Start Portfolio Section
=========================================== -->

<section id="showcase"  class="bg-two">
    <div class="container">
        <div class="row wow fadeInDown" data-wow-duration="500ms">
            <div class="col-lg-12">

                <!-- section title -->
                <div class="title text-center">
                    <h2> <span class="color"> Galería</span></h2>
                    <div class="border"></div>
                </div>
                <!-- /section title -->



            </div> <!-- /end col-lg-12 -->
        </div> <!-- end row -->
    </div>	<!-- end container -->

    <!-- portfolio items -->
    <div class="portfolio-item-wrapper wow fadeInUp" data-wow-duration="500ms">
        <ul id="og-grid2" class="og-grid">
            <?php
            foreach ($fotos['data'] as $foto) {
                $enlace=$foto['link'];
                $texto=str_replace("'","",$foto['caption']['text']).'<br>'.' <a href="'.$enlace.'" target="_blank">Ver en instagram </a>';

                ?>

                <!-- single portfolio item -->
                <li class="mix instagram">
                    <a href="javascript:void(0)" data-vinculo="<?php echo($foto['link']);?>" data-largesrc="<?php echo($foto['images']['standard_resolution']['url']);?>" data-title="Instagram"
                       data-description='<?php echo $texto;?>'>
                        <img src="<?php echo($foto['images']['standard_resolution']['url']);?>" height="650" width="520" alt="Emprendizaje Hispano">
                        <div class="hover-mask">
                                    <span>
									<i class="fa fa-plus fa-2x"></i>
								</span>
                        </div>
                    </a>
                </li>
                <!-- /single portfolio item -->
            <?php } ?>


        </ul> <!-- end og grid -->
    </div>  <!-- portfolio items wrapper -->

    <!-- Videos -->
    <div class="container" style="padding-top:10px;">
        <div class="row">

            <div id="features">
                <div class="item">
                    <div class="features-item">

                        <div class="col-md-6 feature-media media-wrapper wow fadeInUp" data-wow-duration="5ms">
                            <iframe width="560" height="315" src="https://www.youtube.com/embed/_dQFCpKhyX0?rel=0"  border="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                        </div>
                        <div class="col-md-6 feature-desc wow fadeInUp" data-wow-duration="500ms" data-wow-delay="300ms">
                            <h3>Descubre Emprendizaje Hispano</h3>

                            <p style="text-align: justify">En solo un minuto este video explica todo lo que Emprendizaje Hispano tiene que ofrecer.</p>
                            <p style="text-align: justify">Y eso no es todo. Hay mucho más en desarrollo para hacer de nuestra academia una solución integral para los nuevos emprendedores y para quienes ya dieron sus primeros pasos.  </p>
                            <p style="text-align: justify">En Emprendizaje Hispano estamos siempre en la búsqueda de la excelencia. </p>
                        </div>

                    </div>
                </div>
                <div class="item">
                    <div class="features-item">

                        <div class="col-md-6 feature-media media-wrapper wow fadeInUp" data-wow-duration="5ms">
                            <iframe width="560" height="315" src="https://www.youtube.com/embed/V-I8HU3AKFU?rel=0"  border="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                        </div>
                        <div class="col-md-6 feature-desc wow fadeInUp" data-wow-duration="500ms" data-wow-delay="300ms">
                            <h3>Testimonios sobre Emprendizaje Hispano</h3>

                            <p style="text-align: justify">Este video de apenas un minuto de duración explica qué es Emprendizaje Hispano y nos muestra un testimonio positivo de los muchos que cada día expresan nuestros estudiantes.
                            </p>
                            <p style="text-align: justify">En Emprendizaje Hispano queremos ser ejemplo para nuestros seguidores.
                            </p>
                        </div>

                    </div>
                </div>
                <div class="item">
                    <div class="features-item">

                        <div class="col-md-6 feature-media media-wrapper wow fadeInUp" data-wow-duration="5ms">
                            <iframe width="560" height="315" src="https://www.youtube.com/embed/SD-5uQA1ja4?rel=0"  border="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                        </div>
                        <div class="col-md-6 feature-desc wow fadeInUp" data-wow-duration="500ms" data-wow-delay="300ms">
                            <h3>Entrevista en Hispana Realizada</h3>

                            <p style="text-align: justify">Mediante  esta entrevista en solo pocos minutos conocemos el perfil humano y emprendedor de nuestra fundadora Katherine Jimenez Polanco (@katypolanco1).</p>
                            <p style="text-align: justify">El portal Hispana Realizada (@hispanarealizada - @marenasreyes) tuvo la iniciativa de dar a conocer la trayectoria de nuestra creadora reconociendo la importancia de sus emprendimientos.</p>
                            <p style="text-align: justify">En Emprendizaje Hispano queremos ser ejemplo para nuestros seguidores.</p>
                        </div>

                    </div>
                </div>
                <div class="item">
                    <div class="features-item">

                        <div class="col-md-6 feature-media media-wrapper wow fadeInUp" data-wow-duration="5ms">
                            <iframe width="560" height="315" src="https://www.youtube.com/embed/s2rpV_wQrYs?rel=0"  border="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                        </div>

                        <div class="col-md-6 feature-desc wow fadeInUp" data-wow-duration="500ms" data-wow-delay="300ms">
                            <h3>Entrevista en la oficina</h3>
                            <p style="text-align: justify">Disfruta de la entrevista de que fue objeto nuestra fundadora Katherine Jimenez Polanco, de parte de Arianny Calles, de Hecho a mano Atlanta. (@hechoamanoatl - @arianycalles ).</p>
                            <p style="text-align: justify">Allí hablan de varios tópicos empresariales sobre todo de algo tan importante como la creación de empresas y los impuestos.</p>
                            <p style="text-align: justify">En Emprendizaje Hispano, nuestros instructores, te forman  también en impuestos.</p>
                        </div>

                    </div>
                </div>
                <div class="item">
                    <div class="features-item">

                        <div class="col-md-6 feature-media media-wrapper wow fadeInUp" data-wow-duration="5ms">
                            <iframe width="560" height="315" src="https://www.youtube.com/embed/E_Zp3ZoaHK8?rel=0"  border="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                        </div>

                        <div class="col-md-6 feature-desc wow fadeInUp" data-wow-duration="500ms" data-wow-delay="300ms">
                            <h3>Curso de oratoria</h3>
                            <p style="text-align: justify">En el mundo de hoy en día debemos destacarnos como buenos emisores de los mensajes que queremos dejar en la memoria de nuestros clientes; debemos saber expresar de viva voz nuestro pensamiento, para llegar mejor a quienes nos escuchan. Por ello la Oratoria es la herramienta que nunca pasa de moda y que está presente en nuestro menú de cursos y talleres.</p>
                            <p style="text-align: justify">En Emprendizaje Hispano, nuestros instructores, te forman  también en Oratoria. </p>
                        </div>

                    </div>
                </div>
                <div class="item">
                    <div class="features-item">

                        <div class="col-md-6 feature-media media-wrapper wow fadeInUp" data-wow-duration="5ms">
                            <iframe width="560" height="315" src="https://www.youtube.com/embed/yvS5Q3ZEnDM?rel=0"  border="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                        </div>

                        <div class="col-md-6 feature-desc wow fadeInUp" data-wow-duration="500ms" data-wow-delay="300ms">
                            <h3>Curso de QuickBooks</h3>
                            <p style="text-align: justify">Incorpora las modernas formas de administración de pequeños y medianos negocios a tu empresa  través de Quickbooks, la herramienta financiera de hoy.</p>
                            <p style="text-align: justify">Especialmente diseñado pensando en las Pymes,  Quickbooks es la forma más inmediata, sencilla  y económica de llevar control sobre tu negocio: reportes diversos, registro de transacciones financieras y responsabilidades legales, entre muchas otras ventajas más.</p>
                            <p style="text-align: justify">En Emprendizaje Hispano, con nuestros instructores especialistas, te formamos  también en Quickbooks.
                            </p>
                        </div>

                    </div>
                </div>
            </div>


        </div>
        <div class="row">
            <div class="col-md-6 feature-desc wow fadeInUp" data-wow-duration="500ms" data-wow-delay="300ms">
                <a href="https://www.youtube.com/channel/UCB6oC2mA0gqKo52E3EDTArw" target="_blank" class="btn btn-transparent">Ver mas</a>
            </div>

        </div>

    </div>
</section>   <!-- End section -->

<!-- Start Our Team
=========================================== -->

<section id="our-team" class="bg-one">
    <div class="container">
        <div class="row">

            <!-- section title -->
            <div class="title text-center wow fadeIn" data-wow-duration="1500ms" >
                <h2><span class="color"> ¿Quiénes somos?</span></h2>
                <div class="border"></div>
            </div>
            <!-- /section title -->


            <!-- About item -->
            <div class="col-md-12 text-center wow fadeInUp" data-wow-duration="500ms" data-wow-delay="250ms">
                <div class="wrap-about">
                    <div class="icon-box">
                        <i class="fa fa-cubes fa-4x"></i>
                    </div>
                    <!-- Express About Yourself -->
                    <div class="about-content text-center">
                        <h3> <span class=" color_blue"> </span></h3>
                        <p style="text-align: justify" class="color_blue">
                            En Emprendizaje Hispano somos un equipo de profesionales especialistas, artistas, creadores en diferentes disciplinas, comprometidos con nuestra comunidad hispana para impulsar desarrollar, formar y apoyar sus talentos, sus sueños.
                        </p>
                        <p style="text-align: justify" class="color_blue">
                            Reunimos  instructores, cursos y talleres de diferentes índoles, que se adaptan a ti, a tus necesidades específicas. Te daremos herramientas que te ayuden en los diferentes procesos para hacerte crecer, fortalecer y hacer tangible tu proyecto.
                        </p>
                        <p style="text-align: justify" class="color_blue">
                            En Emprendizaje Hispano nuestro personal de excelencia está comprometido a “Empoderarte” en esta nueva era, en nuevos modelos de negocio. Compartimos experiencias y conocimientos para llevarte de lo teórico a lo práctico.
                        </p>
                        <p style="text-align: justify" class="color_blue">
                            Queremos que desde la Plataforma de Emprendizaje Hispano puedas lograr el aprendizaje e intercambio de ideas, de apoyo con tus colegas los soñadores, los microempresarios, emprendedores y empresarios ya establecidos, mediante una red de comunicaciones.
                        </p>
                        <p style="text-align: justify" class="color_blue">
                            En Emprendizaje Hispano, por cada taller o curso, obtendrás certificados y la satisfacción de aprender y compartir con académicos de altura.
                        </p>
                    </div>
                </div>
            </div>
            <!-- End About item -->


        </div> 		<!-- End row -->
    </div>
    <div class="container">
        <div class="row">

            <!-- section title -->
            <div class="title text-center wow fadeInUp" data-wow-duration="500ms">
                <h2><span class="color"> Nuestro equipo</span></h2>
                <div class="border"></div>
            </div>
            <!-- /section title -->

            <!-- team member -->
            <div class="col-md-3 col-sm-6 wow fadeInDown" data-wow-duration="500ms">
                <article class="team-mate kill-margin-bottom">
                    <div class="member-photo">
                        <!-- member photo -->
                        <img class="img-responsive" src="img/team/1.jpg" alt="Katherine Jimenez">
                        <!-- /member photo -->

                        <!-- member social profile -->
                        <div class="mask">
                            <ul class="clearfix">
                                <li><a href="https://www.instagram.com/emprendizaje_hispano/" target="_blank"><i class="fa fa-instagram"></i></a></li>
                                <li><a href="https://twitter.com/Katheri60027162" target="_blank"><i class="fa fa-twitter"></i></a></li>
                                <li><a target="_blank" href="mailto:kathy.polanco13@gmail.com"><i class="fa fa-envelope"></i></a></li>
                                <li><a href="https://www.facebook.com/katherine.ramirez.545" target="_blank"><i class="fa fa-facebook"></i></a></li>
                            </ul>
                        </div>
                        <!-- /member social profile -->
                    </div>

                    <!-- member name & designation -->
                    <div class="member-title">
                        <h4>Katherine Jimenez</h4>
                        <span>Instructora</span>
                    </div>
                    <!-- /member name & designation -->

                    <!-- about member -->
                    <div class="member-info">
                        <p style="text-align: justify" >Es venezolana, Contador Público graduada en la Universidad Santa María, en Caracas, Venezuela. Ha tomado numerosos cursos y talleres relacionados con impuestos, administración de empresas, emprendimiento y otros.</p>
                        <p style="text-align: justify" >Es gerente-propietaria de dos oficinas de la franquicia Daniel Ahart tax service y asesora en asuntos de impuestos.</p>
                        <p style="text-align: justify" >Es fundadora y motor principal de la Academia Emprendizaje Hispano y una de sus más destacadas instructoras en disciplinas tales como Impuestos y Emprendedores Exitosos.</p>
                        <p style="text-align: justify" >Katherine está casada, es madre de tres hijos y es una motivadora nata que ayuda con su ejemplo y experiencia a impulsar las carreras de muchos emprendedores.</p>
                    </div>
                    <!-- /about member -->
                </article>
            </div>
            <!-- end team member -->

            <!-- team member -->
            <div class="col-md-3 col-sm-6 wow fadeInDown" data-wow-duration="500ms" data-wow-delay="200ms">
                <article class="team-mate">
                    <div class="member-photo">
                        <!-- member photo -->
                        <img class="img-responsive" src="img/team/sana.jpg" alt="Sana Hanna">
                        <!-- /member photo -->

                        <!-- member social profile -->
                        <div class="mask">
                            <ul class="clearfix">
                                <li><a target="_blank" href="mailto:sana_rosa@hotmail.com"><i class="fa fa-envelope"></i></a></li>
                            </ul>
                        </div>
                        <!-- /member social profile -->
                    </div>

                    <!-- member name & designation -->
                    <div class="member-title">
                        <h4>MSc. Sana R. Hanna B.</h4>
                        <span>Instructora</span>
                    </div>
                    <!-- /member name & designation -->

                    <!-- about member -->
                    <div class="member-info">
                        <p style="text-align: justify">Economista. MSc en Gerencia de Mercadeo. Diplomado en Coaching. Practitioner PNL, egresada en la Universidad del Zulia, IESA y Universidad Rafael Belloso Chacín. Experiencia en el campo profesional de la Gerencia y Administración, Asesora en Marketing e Implantación de Sistemas de Calidad. Consultora en Desarrollo Organizacional.  Docente de la Universidad del Zulia.  Facilitadora de cursos y talleres de Desarrollo Gerencial en Emprendizaje Hispano y otras empresas. Actualmente Doctorante en Ciencias de la Educación. Productora de comerciales para televisión a través de su empresa Pantalla Naranja Producciones en Maracaibo, Venezuela.</p>
                        <p style="text-align:justify ">
                            <b>Cursos a dictar:</b>
                        </p>
                        <p style="text-align:justify ">
                            <b>Satisfacción del cliente. Clave de éxito: </b> En este curso se exponen los principios de atención al cliente como una de las actividades básicas en cualquier negocio para que este sea exitoso. Temas tales como: ¿Qué busca un cliente? ¿Qué espera el cliente de nosotros? ¿Cómo atender al cliente? ¿Cómo conseguir su fidelidad?, entre muchos otros temas de interés.
                        </p>
                        <p style="text-align:justify ">
                            <b>Administración del tiempo. A veces menos es más:</b> Este curso nos enseña como organizar mejor nuestras actividades en relación con un recurso sumamente escaso en estos días: el tiempo. Establecer prioridades, organizar horarios, distribución de las diversas tareas en función con nuestros compromisos.
                        </p>
                        <p style="text-align:justify ">
                            <b>Negociación. La herramienta de todos y para todo:</b> Los contenidos de este curso están dirigidos a aprender cómo lograr negociaciones del tipo ganar-ganar. Establece los principios sobre los factores a tener en cuenta sobre lo que piensa y desea tu contrario; sobre lo que deseas y necesitas tú y tu empresa como negociador.

                        </p>
                        <p style="text-align:justify ">
                            <b>Los 4 mandamientos del mercadeo:</b> El mercadeo es una herramienta muy necesaria en estos tiempos. Tiene principios y lineamientos que deben respetarse para tener éxito en los negocios. El curso se basa en estas cuatro categorías o mandamientos:
                        </p>
                        <p style="text-align: justify">
                            <i class="fa fa-info-circle fa-xs"></i>
                            <span>Conoce tu empresa</span>
                        </p>
                        <p style="text-align: justify">
                            <i class="fa fa-info-circle fa-xs"></i>
                            <span>Conoce tu cliente</span>
                        </p>
                        <p style="text-align: justify">
                            <i class="fa fa-info-circle fa-xs"></i>
                            <span>Conoce tu producto</span>
                        </p>
                        <p style="text-align: justify">
                            <i class="fa fa-info-circle fa-xs"></i>
                            <span>Conoce tu competencia</span>
                        </p>
                        <p style="text-align:justify ">
                            <b>4 Herramientas para el éxito personal y profesional: </b> Cuatro pilares fundamentales del éxito son la base de este curso. Se exponen y explican las razones por las cuales siempre debes tenerlos en cuenta.
                        </p>
                        <p style="text-align: justify">
                            <i class="fa fa-info-circle fa-xs"></i>
                            <span>Comunicación Efectiva</span>
                        </p>
                        <p style="text-align: justify">
                            <i class="fa fa-info-circle fa-xs"></i>
                            <span>Negociación</span>
                        </p>
                        <p style="text-align: justify">
                            <i class="fa fa-info-circle fa-xs"></i>
                            <span>Gerencia</span>
                        </p>
                        <p style="text-align: justify">
                            <i class="fa fa-info-circle fa-xs"></i>
                            <span>Mercadeo</span>
                        </p>
                    </div>
                    <!-- /about member -->

                </article>
            </div>
            <!-- end team member -->

            <!-- team member -->
            <div class="col-md-3 col-sm-6 wow fadeInDown" data-wow-duration="500ms" data-wow-delay="300ms">
                <article class="team-mate">
                    <div class="member-photo">
                        <!-- member photo -->
                        <img class="img-responsive" src="img/team/malena.jpg" alt="Malena Soto de Valero">
                        <!-- /member photo -->

                        <!-- member social profile -->
                        <div class="mask">
                            <ul class="clearfix">
                                <li><a target="_blank" href="mailto:mchsoto@hotmail.com"><i class="fa fa-envelope"></i></a></li>
                            </ul>
                        </div>
                        <!-- /member social profile -->
                    </div>

                    <!-- member name & designation -->
                    <div class="member-title">
                        <h4>MSC. Malena Soto de Valero</h4>
                        <span>Instructora</span>
                    </div>
                    <!-- /member name & designation -->

                    <!-- about member -->
                    <div class="member-info">
                        <p style="text-align: justify">MSc. Gerencia de Mercadeo. Diplomado en Coaching. Practitioner PNL, Experiencia en el campo profesional de la comunicación social: Producción de material audiovisual (radio y TV) e impresos. Conductora de programas de radio y TV. Asesora en marketing social y desarrollo electoral. Docente en materias de pregrado y postgrado de comunicación social en sistemas presenciales y a distancia. Facilitadora de cursos y talleres de desarrollo humano integral. Actualmente Doctorante en Ciencias de la Educación. Miembro permanente del staff de Emprendizaje Hispano.</p>
                        <p style="text-align:justify ">
                            <b>Cursos a dictar:</b>
                        </p>
                        <p style="text-align:justify ">
                            <b>Elementos básicos de la Oratoria:</b> El taller incluye la teoría y la práctica con las herramientas básicas de la oratoria: respiración diafragmática, manejo de la voz, buena dicción, pronunciación. Ejercicios básicos.
                        </p>
                        <p style="text-align:justify ">
                            <b>Presentaciones Orales exitosas:</b> Acabar con el miedo escénico es uno de los objetivos de este curso que presenta técnicas y prácticas concretas para mejorar la capacidad expresiva y habilidades de presentación oral. Se centra en aspectos vitales para manejarse como un orador exitoso.
                        </p>
                        <p style="text-align:justify ">
                            <b>Atención al Cliente:</b> El cliente siempre tiene la razón y es la razón de ser de las empresas,  por ello el curso contempla las herramientas técnicas necesarias para el manejo de clientes, reconocimiento de los tipos de cliente, manejo de emociones y atención personalizada.
                        </p>
                        <p style="text-align:justify ">
                            <b>El Arte de ser Feliz:</b> En un mundo de permanentes cambios repentinos nos vemos abrumados por la realidad. En este curso se manejan los elementos básicos en el manejo de las emociones y conocimiento de la Psicología en el desarrollo de la actitud positiva que le oriente a canalizar su motivación y encaminarse hacia el éxito.
                        </p>
                        <p style="text-align:justify ">
                            <b>Gerencia de Talento Influyente:</b> Las relaciones entre las personas son básicas en cualquier organización que quiera trascender en el tiempo y en los negocios. En el curso se dan a conocer las mejores herramientas para una gerencia abierta y horizontal que mejora las relaciones interpersonales y además ayuda a reconocer competencias para convertir los RRHH en talentos influyentes con garantía de éxito y desarrollo.
                        </p>

                    </div>
                    <!-- /about member -->
                </article>
            </div>
            <!-- end team member -->

            <!-- team member -->
            <div class="col-md-3 col-sm-6 wow fadeInDown" data-wow-duration="500ms" data-wow-delay="400ms">
                <article class="team-mate">
                    <div class="member-photo">
                        <!-- member photo -->
                        <img class="img-responsive" src="apple-touch-icon.png" alt="Bernardo Jóse Jiménez Panciera" width="280" height="280">
                        <!-- /member photo -->

                        <!-- member social profile -->
                        <div class="mask">
                            <ul class="clearfix">
                                <li><a href="https://www.instagram.com/panciera1/" target="_blank"><i class="fa fa-instagram"></i></a></li>
                                <li><a target="_blank" href="mailto:bernardojimenez21@gmail.com"><i class="fa fa-envelope"></i></a></li>
                                <li><a target="_blank" href="https://www.facebook.com/bernardo.jimenez.3511"><i class="fa fa-facebook"></i></a></li>
                            </ul>
                        </div>
                        <!-- /member social profile -->
                    </div>

                    <!-- member name & designation -->
                    <div class="member-title">
                        <h4>Bernardo Jóse Jimenez Panciera</h4>
                        <span>Instructor</span>
                    </div>
                    <!-- /member name & designation -->

                    <!-- about member -->
                    <div class="member-info">
                        <p style="text-align: justify">Comunicador social (Universidad del Zulia, Venezuela, 1994), miembro fundador del staff de instructores de Emprendizaje Hispano, con una vasta experiencia en las distintas plazas de la dirección técnica de estudio de Television y en la dirección técnica de control maestro de television. Ha trabajado en empresas tales como CNN en español, en Atlanta; Encompass Digital Media, también en Atlanta, GA y en canales de televisión regional en Estados Unidos, tanto en el área técnica como en la producción de programas de televisión y escritura de guiones.</p>
                        <p style="text-align:justify ">
                            <b>Charlas:</b>
                        </p>
                        <p style="text-align:justify ">
                            “Tareas y habilidades requeridas por un director técnico de televisión en canales y en empresas de servicio”. En solo 45 minutos, Bernardo Jimenez nos pone al día en la descripción y enumeración de las habilidades necesarias que debe tener un profesional en esa área.
                        </p>
                        <p style="text-align:justify ">
                            La charla está dirigida a estudiantes de comunicación social y técnicos del área.
                        </p>
                    </div>
                    <!-- /about member -->
                </article>
            </div>
            <!-- end team member -->

        </div>  	<!-- End row -->

        <div class="row" style="padding-top: 10px">

            <!-- team member -->
            <div class="col-md-3 col-sm-6 wow fadeInDown" data-wow-duration="500ms" data-wow-delay="500ms">
                <article class="team-mate">
                    <div class="member-photo">
                        <!-- member photo -->
                        <img class="img-responsive" src="img/team/cristina.jpg" alt="Cristina Mayz">
                        <!-- /member photo -->

                        <!-- member social profile -->
                        <div class="mask">
                            <ul class="clearfix">
                                <li><a href="https://www.facebook.com/chistimg" target="_blank"><i class="fa fa-facebook"></i></a></li>
                            </ul>
                        </div>
                        <!-- /member social profile -->
                    </div>

                    <!-- member name & designation -->
                    <div class="member-title">
                        <h4>Cristina Mayz</h4>
                        <span>Crecimiento y Desarrollo Personal</span>
                    </div>
                    <!-- /member name & designation -->

                    <!-- about member -->
                    <div class="member-info">
                        <p style="text-align: justify">Licenciada en Psicología. Experiencia como Psicóloga en el área escolar, deficiencias del aprendizaje, terapia familiar, inteligencia emocional y bienestar personal.</p>
                    </div>
                    <!-- /about member -->
                </article>
            </div>
            <!-- end team member -->
            <!-- team member -->
            <div class="col-md-3 col-sm-6 wow fadeInDown" data-wow-duration="500ms" data-wow-delay="600ms">
                <article class="team-mate">
                    <div class="member-photo">
                        <!-- member photo -->
                        <img class="img-responsive" src="img/team/member-5.jpg" alt="Nancy Díaz">
                        <!-- /member photo -->

                        <!-- member social profile -->
                        <div class="mask">
                            <ul class="clearfix">
                                <li><a href="https://www.instagram.com/nancydiaz9121/" target="_blank"><i class="fa fa-instagram"></i></a></li>
                                <li><a href="https://twitter.com/@turupin" target="_blank"><i class="fa fa-twitter"></i></a></li>
                                <li><a target="_blank" href="mailto:badnaservices@gmail.com"><i class="fa fa-envelope"></i></a></li>
                                <li><a target="_blank" href="https://www.facebook.com/nancy.diazjaimes"><i class="fa fa-facebook"></i></a></li>
                            </ul>
                        </div>
                        <!-- /member social profile -->
                    </div>

                    <!-- member name & designation -->
                    <div class="member-title">
                        <h4>Nancy Díaz</h4>
                        <span>Instructora</span>
                    </div>
                    <!-- /member name & designation -->

                    <!-- about member -->
                    <div class="member-info">
                        <p style="text-align: justify"> Ingeniero Civil (Universidad Central de Venezuela 1983) con especialización en cálculos estructurales y desarrollo de proyectos.; Profesora de “Excel aplicado al desarrollo de un plan financiero para emprendedoras en el programa de Mujeres y Negocios”. Es experta en la elaboración de Certificaciones Laborales como un medio de ayuda a inmigrantes Hispanos, para vivir y trabajar legalmente en Estados Unidos (2017-presente).Es voluntaria en La Asociación Latino Americana de Atlanta, GA. Es fundadora de la Academia Emprendizaje Hispano y mantiene la siguiente actividad docente:</p>

                        <p style="text-align:justify;">
                            <b>Cursos a dictar:</b>
                        </p>
                        <p style="text-align:justify ">
                            <b>Excel básico, medio y avanzado:</b>  Dirigido a todas aquellas personas que como emprendedores necesitan conocer las herramientas para la elaboración de un plan financiero para su negocio, el curso incluye la elaboración práctica de una tabla financiera de un modelo de negocio.
                        </p>
                        <p style="text-align:justify ">
                            <b>Aprende a crear y a usar las redes sociales:</b> Dirigido a todos aquellos emprendedores que para promocionar su negocio deben usar las redes sociales como: Instagram, Facepage, Twitter, correos electrónicos y manejo de envío y recepción de archivos; totalmente práctico.
                        </p>
                        <p style="text-align:justify ">
                            <b>Charlas:</b>
                        </p>
                        <p style="text-align:justify ">
                            <b>Cómo vivir y trabajar legalmente en Estados Unidos:</b> Dirigido a todos aquellos inmigrantes que desconocen que la ley contempla la posibilidad de otorgar una residencia permanente a cambio de trabajo.

                        </p>

                    </div>
                    <!-- /about member -->
                </article>
            </div>
            <!-- end team member -->
            <!-- team member -->
            <div class="col-md-3 col-sm-6 wow fadeInDown" data-wow-duration="500ms" data-wow-delay="700ms">
                <article class="team-mate">
                    <div class="member-photo">
                        <!-- member photo -->
                        <img class="img-responsive" src="img/team/mari.jpg" alt="María Elena Díaz Jaimes">
                        <!-- /member photo -->

                        <!-- member social profile -->
                        <div class="mask">
                            <ul class="clearfix">
                                <li><a href="https://www.instagram.com/profemedj/" target="_blank"><i class="fa fa-instagram"></i></a></li>
                                <li><a href="https://twitter.com/@profemedj" target="_blank"><i class="fa fa-twitter"></i></a></li>
                                <li><a target="_blank" href="mailto:Medj1212@gmai.com"><i class="fa fa-envelope"></i></a></li>
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            </ul>
                        </div>
                        <!-- /member social profile -->
                    </div>

                    <!-- member name & designation -->
                    <div class="member-title">
                        <h4>María Elena Díaz Jaimes</h4>
                        <span>Instructora</span>
                    </div>
                    <!-- /member name & designation -->

                    <!-- about member -->
                    <div class="member-info">
                        <p style="text-align: justify">Profesora en la especialidad de castellano, literatura y latín (1984); magister en lingüística (1993); doctora en cultura latinoamericana y caribeña (2012); Es personal académico de la universidad pedagógica experimental libertador (UPEL) en Venezuela; miembro del Instituto Venezolano de Investigaciones Lingüísticas y Literarias Andrés Bello” (IVILLAB) e investigadora con publicaciones y ponencias sobre enseñanza de la lengua y análisis del discurso. Es una de nuestras instructoras de ingreso reciente a Emprendizaje Hispano.</p>
                        <p style="text-align:justify ">
                            <b> Cursos a dictar:</b>
                        </p>
                        <p style="text-align:justify ">
                            <b>Elaboración de guiones:</b> Dirigido a todas aquellas personas interesadas en conocer los principios, técnicas y realización de guiones para programas de radio, televisión y medios afines. Es una herramienta esencial para todas aquellas personas motivadas a la producción profesional en estos medios.
                        </p>

                    </div>
                    <!-- /about member -->
                </article>
            </div>
            <!-- end team member -->
            <!-- team member -->
            <div class="col-md-3 col-sm-6 wow fadeInDown" data-wow-duration="500ms" data-wow-delay="800ms">
                <article class="team-mate">
                    <div class="member-photo">
                        <!-- member photo -->
                        <img class="img-responsive" src="img/team/joseluis.jpg" alt="José Luis Angarita">
                        <!-- /member photo -->

                        <!-- member social profile -->
                        <div class="mask">
                            <ul class="clearfix">
                                <li><a href="https://www.instagram.com/angaritaavilajoseluis/" target="_blank"><i class="fa fa-instagram"></i></a></li>
                            </ul>
                        </div>
                        <!-- /member social profile -->
                    </div>

                    <!-- member name & designation -->
                    <div class="member-title">
                        <h4>José Luis Angarita</h4>
                        <span>Instructor</span>
                    </div>
                    <!-- /member name & designation -->

                    <!-- about member -->
                    <div class="member-info">
                        <p style="text-align: justify"> Licenciado en Comunicación Social, Profesor en el área audiovisual en la Escuela de Comunicación Social de la Universidad del Zulia, Venezuela. Director y productor de comerciales para la televisión y realizador de documentales industriales y corporativos. Autor de “Las reinas de Carnaval visten de rosado” (cuentos) y “Cabello de ángel” (teatro). Miembro permanente del equipo de Emprendizaje Hispano.</p>
                        <p style="text-align:justify ">
                            <b> Cursos a dictar:</b>
                        </p>
                        <p style="text-align:justify ">
                            <b>Producción y dirección de comerciales para la televisión:</b> Si siempre quisiste conocer cómo se concibe, planifica y realiza un comercial para la televisión esta es una buena oportunidad. ¿Qué es un comercial para la TV? ¿Cómo se planifica? ¿Quién es quién en la producción? ¿Qué necesitamos para hacerlo? ¿Quién nos da la idea? Las respuestas a esas preguntas y muchas más las conocerás en nuestro curso.
                        </p>

                    </div>
                    <!-- /about member -->
                </article>
            </div>
            <!-- end team member -->

        </div>
        <!-- End row -->
        <div class="row" style="padding-top: 10px">
            <!-- team member -->
            <div class="col-md-3 col-sm-6 wow fadeInDown" data-wow-duration="500ms" data-wow-delay="900ms">
                <article class="team-mate">
                    <div class="member-photo">
                        <!-- member photo -->
                        <img class="img-responsive" src="img/team/mariet.jpeg" alt="Mariet Ostos">
                        <!-- /member photo -->

                        <!-- member social profile -->
                        <div class="mask">
                            <ul class="clearfix">
                                <li><a href="https://www.instagram.com/marietostos/" target="_blank"><i class="fa fa-instagram"></i></a></li>
                                <li><a href="https://twitter.com/mostos13?lang=es" target="_blank"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="http://www.marietostos.com/" target="_blank"><i class="fa fa-globe"></i></a></li>
                                <li><a target="_blank" href="mailto:info@marietostos.com"><i class="fa fa-envelope"></i></a></li>
                                <li><a href="https://www.facebook.com/success.orlando" target="_blank"><i class="fa fa-facebook"></i></a></li>
                            </ul>
                        </div>
                        <!-- /member social profile -->
                    </div>

                    <!-- member name & designation -->
                    <div class="member-title">
                        <h4>Mariet Ostos</h4>
                        <span>Instructora</span>
                    </div>
                    <!-- /member name & designation -->

                    <!-- about member -->
                    <div class="member-info">
                        <p style="text-align: justify"> Soñadora, trabajadora, motivadora, líder; son sólo algunas palabras que describen quién soy como persona. Mi nombre es Mariet Ostos, crecí en Caracas, Venezuela, decidí seguir los pasos y el legado de mi madre para estudiar Contabilidad. </p>
                        <p style="text-align: justify">El 16 de enero de 2000 descubrí que estaba embarazada. Esto había cambiado mi vida entera y comprometido todo lo que tenía en ese momento. Con las condiciones en las que estaba mi país, tuve que tomar una decisión para el futuro de mi hija. Tuve que resolver el problema rápidamente, así que decidí mudarme a otro país, sola. Yo había visitado Atlanta, Georgia, anteriormente,  pero cuando me pusieron en la posición de elegir, sabía que era allí,  donde yo quería vivir. </p>
                        <p style="text-align: justify">Me mudé con mil dólares en mi bolsillo y un bulto lleno de sueños. Viví en un refugio durante 6 meses, la señora que estaba a cargo me ofreció un lugar para vivir a cambio de trabajo porque no tenía documentos legales en ese momento para trabajar, esa fue la primera vez que alguien me dio una oportunidad, me dio esperanza y determinación para seguir adelante. Viví allí durante 6 meses hasta que me di cuenta que tenía que salir y comenzar mi propio viaje. </p>
                        <p style="text-align: justify">Fue un duro comienzo, limpié casas, baños y oficinas. No importa lo que hice, nunca perdí el foco de mis metas de tener mi propio negocio. Nunca dejé de soñar; renunciar nunca fue una opción. Después de años de estudiar la ley tributaria, en mayo de 2007 hice el primer paso y comencé mi propio servicio de preparación de impuestos en Atlanta; en 2009 fui contratada para hacer un estudio de mercado del estado de Florida; Orlando era una ciudad en rápido crecimiento y no dudé en mudarme y prosperar en esta nueva ciudad. </p>
                        <p style="text-align: justify">Me mudé a Orlando en 2010, lista para ayudar a una nueva comunidad y ampliar mis horizontes. Tuve que empezar de cero, golpear puertas explicando mis servicios, y difundir mi nombre. Alrededor de 5 años más tarde, me di cuenta de que una cantidad numerosa de personas comenzaron a venir a mi oficina para consejo y apoyo. Decidí hacer un cambio y crear un impacto en la gente en mi vida. En el 2014 recibí mi licencia de Coaching Personal, posteriormente recibí un certificado de Inteligencia Emocional y luego me inspiré para unirme al equipo de John Maxwell. Esta era una manera de obtener el conocimiento que necesitaba para enseñar liderazgo y establecer mis estándares, aprendí maneras sobre cómo dar herramientas para vivir más feliz y libre de estrés. </p>
                        <p style="text-align: justify">En 2015 fui reconocida como empresario del año de la sociedad de la inteligencia emocional. A principios de 2016, recibí el mejor premio de oficina de preparación de impuestos, dado por el mejor negocio de Orlando y seguimos siéndolo hasta el 2018. La adquisición de estos premios ha impactado positivamente mi impulso para ayudar a los demás. Más tarde en 2016, decidí iniciar clases de liderazgo y motivación para influir en mi comunidad. </p>
                        <p style="text-align: justify">Ver a la gente que amo, que logren sus sueños y darles la esperanza que necesitan para avanzar es la sensación más maravillosa. Continuaré día a día ayudando a mi gente dándoles poder, sueños y las herramientas para entender que si te amas a ti mismo puedes hacer cualquier cosa que tu corazón desee, ese es mi legado.</p>
                        <p style="text-align:justify ">
                            <b> Cursos a dictar:</b>
                        </p>
                        <p style="text-align:justify ">

                        </p>

                    </div>
                    <!-- /about member -->
                </article>
            </div>
            <!-- end team member -->
            <!-- team member -->
            <div class="col-md-3 col-sm-6 wow fadeInDown" data-wow-duration="500ms" data-wow-delay="1000ms">
                <article class="team-mate">
                    <div class="member-photo">
                        <!-- member photo -->
                        <img class="img-responsive" src="img/team/Marle.jpg" alt="Marle Fabiola González">
                        <!-- /member photo -->

                        <!-- member social profile -->
                        <div class="mask">
                            <ul class="clearfix">
                                <li><a href="https://www.instagram.com/angaritaavilajoseluis/" target="_blank"><i class="fa fa-instagram"></i></a></li>
                            </ul>
                        </div>
                        <!-- /member social profile -->
                    </div>

                    <!-- member name & designation -->
                    <div class="member-title">
                        <h4>Marle Fabiola González</h4>
                        <span>Instructora</span>
                    </div>
                    <!-- /member name & designation -->

                    <!-- about member -->
                    <div class="member-info">
                        <p style="text-align: justify"> Venezolana, Licenciada en Publicidad y Relaciones Públicas con un Máster en Gerencia de Empresas, mención Mercadeo; Certificada Google; con más de 10 años de experiencia en Marketing y marketing digital. Fundadora de MKT Group y Mujer Magazine.</p>
                        <p style="text-align:justify ">
                            <b> Cursos a dictar:</b>
                        </p>
                        <p style="text-align:justify ">
                            <b>Redes sociales:</b>
                        </p>
                        <p style="text-align:justify ">
                            <i class="fa fa-info-circle fa-xs"></i><span>Introducción: Redes sociales, cuáles son y cuál es su importancia.</span>
                        </p>
                        <p style="text-align: justify">
                            <i class="fa fa-info-circle fa-xs"></i><span>Planificación y estrategia: Elección de las redes a utilizar</span>
                        </p>
                        <p style="text-align: justify">
                            <i class="fa fa-info-circle fa-xs"></i><span>Configuración básica. Planificación mensual / Manual de contenido / Grilla</span>
                        </p>
                        <p style="text-align:justify ">
                            <b>Facebook:</b>
                        </p>
                        <p style="text-align: justify">
                            <i class="fa fa-info-circle fa-xs"></i><span>Diferencia entre fan page, perfil y grupos.</span>
                        </p>
                        <p style="text-align: justify">
                            <i class="fa fa-info-circle fa-xs"></i><span>Nombre de usuario y verificación de la página.</span>
                        </p>
                        <p style="text-align: justify">
                            <i class="fa fa-info-circle fa-xs"></i><span>Roles.  Panel de administración. Personalización. Publicación. Mensajería.</span>
                        </p>
                        <p style="text-align: justify">
                            <i class="fa fa-info-circle fa-xs"></i><span>Consigue tus primeros likes. Estadísticas. Facebook Ads.</span> 
                        </p>
                        <p style="text-align:justify ">
                            <b>Instagram:</b>
                        </p>
                        <p style="text-align: justify">
                            <i class="fa fa-info-circle fa-xs"></i>
                            <span>Ventajas del uso de Instagram.</span>
                        </p>
                        <p style="text-align: justify">
                            <i class="fa fa-info-circle fa-xs"></i>
                            <span>Creación y administración de una cuenta para empresa. Consejos y trucos.</span>
                        </p>
                        <p style="text-align: justify">
                            <i class="fa fa-info-circle fa-xs"></i>
                            <span>Perfil, biografía. Fotos / videos.</span>
                        </p>
                        <p style="text-align: justify">
                            <i class="fa fa-info-circle fa-xs"></i>
                            <span>Hashtags. Geotags. Stories.</span>
                        </p>
                        <p style="text-align: justify">
                            <i class="fa fa-info-circle fa-xs"></i>
                            <span>Instagram shopping.Estadísticas.</span>
                        </p> 
                        <p style="text-align: justify">
                            <i class="fa fa-info-circle fa-xs"></i><span>Campañas (creación, manejo, resultados).</span>
                        </p>
                    </div>
                    <!-- /about member -->
                </article>
            </div>
            <!-- end team member -->

        </div>


    </div>   	<!-- End container -->
</section>   <!-- End section -->

<!-- Srart Contact Us
=========================================== -->
<section id="contact-us" class="bg-two">
    <div class="container">
        <div class="row">

            <!-- section title -->
            <div class="title text-center wow fadeIn" data-wow-duration="500ms">
                <h2><span class="color">Contacto</span></h2>
                <div class="border"></div>
            </div>
            <!-- /section title -->

            <!-- Contact Details -->
            <div class="contact-info col-md-6 wow fadeInUp" data-wow-duration="500ms">
                <div class="contact-details">
                    <div class="con-info clearfix">
                        <i class="fa fa-home fa-lg"></i>
                        <span>5670 Atlanta Hwy Suite A3, Alpharetta Ga 30004</span>
                    </div>

                    <div class="con-info clearfix">
                        <a href="tel:+1828539133">
                            <i class="fa fa-phone fa-lg"></i>
                            <span>828-539-133</span></a>
                    </div>

                    <div class="con-info clearfix">
                        <i class="fa fa-fax fa-lg"></i>
                        <span>678-609-4364</span>
                    </div>
                    <div class="con-info clearfix">
                        <i class="fa fa-envelope fa-lg"></i>
                        <span>info@emprendizajehispano.com</span>
                    </div>
                    <div class="con-info clearfix">
                        <i class="fa fa-envelope fa-lg"></i>
                        <span>daanbsolution@gmail.com (El aréa de asesoría)</span>
                    </div>
                </div>
            </div>
            <!-- / End Contact Details -->

            <!-- Contact Form -->
            <div class="contact-form col-md-6 wow fadeInUp" data-wow-duration="500ms" data-wow-delay="300ms">
                <form id="contact-form" method="post" action="sendmail.php" role="form">

                    <div class="form-group">
                        <input type="text" placeholder="Nombre y Apellido" class="form-control" name="name" id="name">
                    </div>

                    <div class="form-group">
                        <input type="email" placeholder="Email" class="form-control" name="email" id="email">
                    </div>

                    <div class="form-group">
                        <input type="text" placeholder="Asunto" class="form-control" name="subject" id="subject">
                    </div>

                    <div class="form-group">
                        <textarea rows="6" placeholder="Mensaje" class="form-control" name="message" id="message"></textarea>
                    </div>

                    <div id="mail-success" class="success">
                        Muchas Gracias. El mensaje fue enviado :)
                    </div>

                    <div id="mail-fail" class="error">
                        Disculpe, Ha ocurrido un error. Intente mas tarde :(
                    </div>

                    <div id="cf-submit">
                        <input type="submit" id="contact-submit" class="btn btn-transparent" value="Enviar">
                    </div>

                </form>
            </div>
            <!-- ./End Contact Form -->

        </div> <!-- end row -->
    </div> <!-- end container -->

    <!-- Google Map -->
    <div class="google-map wow fadeInDown" data-wow-duration="25ms">
        <div id="map-canvas"></div>
    </div>
    <!-- /Google Map -->

</section> <!-- end section -->

<!-- end Contact Area
========================================== -->

<footer id="footer" class="bg-one">
    <div class="container">
        <div class="row wow fadeInUp" data-wow-duration="1ms">
            <div class="col-lg-12">

                <!-- Footer Social Links -->
                <div class="social-icon">
                    <ul>
                        <li><a href="https://www.facebook.com/katherine.ramirez.545" target="_blank"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="https://twitter.com/Katheri60027162" target="_blank"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="https://www.instagram.com/emprendizaje_hispano/" target="_blank"><i class="fa fa-instagram"></i></a></li>
                        <li><a href="https://www.youtube.com/channel/UCB6oC2mA0gqKo52E3EDTArw" target="_blank"><i class="fa fa-youtube-play"></i></a></li>
                        <li><a href="#" class="whatsapp" data-telefono="+14049929301"><i class="fa fa-whatsapp"></i></a></li>
                    </ul>
                </div>
                <!--/. End Footer Social Links -->

                <!-- copyright -->
                <div class="copyright text-center">


                    <p class="color_blue">Copyright © 2019 Emprendizaje Hispano - Todos los derechos reservados - Desarrollado por <a href="http://www.rendon.com.ve"><img src="img/logo_m.png" height="20"></a></p>
                </div>
                <!-- /copyright -->

            </div> <!-- end col lg 12 -->
        </div> <!-- end row -->
    </div> <!-- end container -->
</footer> <!-- end footer -->

<!-- Back to Top
============================== -->

<a href="#" id="scrollUp">
    <i class="fa fa-angle-up fa-2x"></i>
</a>

<!-- end Footer Area
========================================== -->

<!--
Essential Scripts
=====================================-->

<!-- Main jQuery -->
<script src="js/jquery-1.11.0.min.js"></script>
<!-- Bootstrap 3.1 -->
<script src="js/bootstrap.min.js"></script>
<!-- Slitslider -->
<script src="js/jquery.slitslider.js"></script>
<script src="js/jquery.ba-cond.min.js"></script>
<!-- Parallax -->
<script src="js/jquery.parallax-1.1.3.js"></script>
<!-- Owl Carousel -->
<script src="js/owl.carousel.min.js"></script>
<!-- Portfolio Filtering -->
<script src="js/jquery.mixitup.min.js"></script>
<!-- Custom Scrollbar -->
<script src="js/jquery.nicescroll.min.js"></script>
<!-- Jappear js -->
<script src="js/jquery.appear.js"></script>
<!-- Pie Chart -->
<script src="js/easyPieChart.js"></script>
<!-- jQuery Easing -->
<script src="js/jquery.easing-1.3.pack.js"></script>
<!-- tweetie.min -->
<!--<script src="js/tweetie.min.js"></script>-->
<!-- Google Map API -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAS_Terb4zrSoSa8bt6yEw8yyBYuHGu0Ik">

</script>
<!-- Highlight menu item -->
<script src="js/jquery.nav.js"></script>
<!-- Sticky Nav -->
<script src="js/jquery.sticky.js"></script>
<!-- Number Counter Script -->
<script src="js/jquery.countTo.js"></script>
<!-- wow.min Script -->
<script src="js/wow.min.js"></script>
<!-- For video responsive -->
<script src="js/jquery.fitvids.js"></script>
<!-- Grid js -->
<script src="js/grid.js"></script>
<!-- Custom js -->
<script src="js/custom.js"></script>

</body>
</html>